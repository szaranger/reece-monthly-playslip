# Reece Monthly Payslip

This project was bootstrapped with [Create React App]

## Install NPM packages

Install NPM packages for the project

```
npm install
```

## Run the App

- Run the following command

```
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

- Select the provided file _income.csv_ (it's in the project's root directory) via the **Choose file** button
- Once the results appear on the page, click **Download** button. A CSV file called _output.csv_ will be saved to the computer

## Tests

Run the unit tests with the following command

```
yarn test
```

Launches the test runner in the interactive watch mode.
