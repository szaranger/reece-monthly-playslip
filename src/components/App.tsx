import React, { useState } from "react";
import FileInput from "./FileInput";
import PaySlip from "./PaySlip";
import { parse } from "../lib/parse";
import "../styles/App.css";

function App() {
  const [data, setData] = useState([]);

  const onUpdate = (data: any) => {
    setData(data);
  };

  const details = parse(data);

  const handleClick = () => {
    const csvContent = details as unknown;
    // @ts-ignore
    const blob = new Blob([csvContent], {
      type: "text/csv;charset=utf-8;",
    });

    const url = URL.createObjectURL(blob);

    const pom = document.createElement("a");
    pom.href = url;
    pom.setAttribute("download", "output.csv");
    pom.click();
  };

  return (
    <div className="App">
      <h1>Employee Monthly Payslip</h1>
      <FileInput onUpdate={onUpdate} />
      {details.length > 0 && <PaySlip details={details} />}
      {details.length > 0 && (
        <button className="App-button" onClick={handleClick}>
          Download
        </button>
      )}
    </div>
  );
}

export default App;
