import React from "react";
import { render, screen } from "@testing-library/react";
import PaySlip from "../PaySlip";

const props = {
  details: [
    ["David Rudd", "01 March - 31 March", 5004, 922, 4082, 450],
    ["Ryan Chen", "01 March - 31 March", 10000, 2696, 7304, 1000],
  ],
};

test("renders heading", () => {
  render(<PaySlip {...props} />);

  expect(screen.getByText(/Name/i)).toBeInTheDocument();
  expect(screen.getByText(/Pay Period/i)).toBeInTheDocument();
  expect(screen.getByText(/Gross Income/i)).toBeInTheDocument();
  expect(screen.getByText(/Income Tax/i)).toBeInTheDocument();
  expect(screen.getByText(/Super/i)).toBeInTheDocument();
});

test("renders items correctly", () => {
  render(<PaySlip {...props} />);

  expect(screen.getByText(/David Rudd/i)).toBeInTheDocument();
  expect(screen.getByText(/5004/i)).toBeInTheDocument();
  expect(screen.getByText(/922/i)).toBeInTheDocument();
  expect(screen.getByText(/4082/i)).toBeInTheDocument();
});
