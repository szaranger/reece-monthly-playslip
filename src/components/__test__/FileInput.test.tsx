import React from "react";
import { render, screen } from "@testing-library/react";
import FileInput from "../FileInput";

test("renders file input", () => {
  const { container } = render(<FileInput onUpdate={() => {}} />);

  expect(
    screen.getByText(/Select a CSV file with payment details/i)
  ).toBeInTheDocument();
  expect(
    container.querySelector("#react-csv-reader-input")
  ).toBeInTheDocument();
});
