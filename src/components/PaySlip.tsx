import React from "react";
import "../styles/PaySlip.css";

function PaySlip({ details }: { details: any }) {
  return (
    <div className="container">
      <table className="table">
        <tbody>
          <tr>
            <th>Name</th>
            <th>Pay Period</th>
            <th>Gross Income</th>
            <th>Income Tax</th>
            <th>Super</th>
          </tr>
          {details.map((item: any) => (
            <tr key={item[0]}>
              <td>{item[0]}</td>
              <td>{item[1]}</td>
              <td>${item[2]}</td>
              <td>${item[3]}</td>
              <td>${item[4]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default PaySlip;
