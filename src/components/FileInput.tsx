import React, { useState } from "react";
import CSVReader from "react-csv-reader";
import "../styles/FileInput.css";

const options = {
  header: false,
  dynamicTyping: true,
  skipEmptyLines: true,
  transformHeader: (header: string) => header,
};

function FileInput({ onUpdate }: { onUpdate: (data: any) => void }) {
  const [message, setMessage] = useState("");

  const handleFileLoaded = (data: any, fileInfo: any) => {
    console.log(data);
    onUpdate(data);
    const { name, size, type } = fileInfo;
    setMessage(`Filename: ${name}, Size: ${size}kb, Type: ${type}`);
  };

  return (
    <div className="container">
      <CSVReader
        cssClass="react-csv-input"
        label="Select a CSV file with payment details"
        onFileLoaded={handleFileLoaded}
        parserOptions={options}
      />
      <p>{message}</p>
    </div>
  );
}

export default FileInput;
