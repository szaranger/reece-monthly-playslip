export const rules = [
  { amount: 18_200, min: 0, fixed: 0, rate: 0 },
  { amount: 37_000, min: 18_201, fixed: 0, rate: 0.19 },
  { amount: 80_000, min: 37_001, fixed: 3_572, rate: 0.325 },
  { amount: 180_000, min: 80_001, fixed: 17_547, rate: 0.37 },
  { amount: 1000_000_000_000, min: 180_001, fixed: 54_547, rate: 0.45 },
];
