import { calc } from "../calc";

test("calculates tax correctly", () => {
  expect(Math.round(calc(60050))).toBe(922);
  expect(Math.round(calc(120000))).toBe(2696);
});
