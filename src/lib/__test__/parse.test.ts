import { parse } from "../parse";

test("parses css input correctly", () => {
  const input = [
    ["David", "Rudd", 60050, "9%", "01 March - 31 March"],
    ["Ryan", "Chen", 120000, "10%", "01 March - 31 March"],
  ];

  const expected = [["David Rudd", "01 March - 31 March", 5004, 922, 4082, 450], ["Ryan Chen", "01 March - 31 March", 10000, 2696, 7304, 1000]];

  expect(parse(input)).toEqual(expected);
});
