import { calc } from "../lib/calc";

export function parse(input = []): any[] {
  let output: any = [];

  input.forEach((data: any) => {
    const name = `${data[0]} ${data[1]}`;
    const salary = data[2];
    const superannuation = data[3].replace("%", "");
    const period = data[4];

    const tax = Math.round(calc(salary));

    const gross = Math.round(salary / 12);
    const net = Math.round(gross - tax);
    const sup = Math.round((gross * superannuation) / 100);

    output.push([name, period, gross, tax, net, sup]);
  });

  return output;
}
