import { rules } from "../constants/rules";

const input: any = [
  ["David", "Rudd", 60050, "9%", "01 March - 31 March"],
  ["Ryan", "Chen", 120000, "10%", "01 March - 31 March"],
];

export function calc(salary: number) {
  const orderedRules = rules.sort((a, b) => (a.amount > b.amount ? 1 : -1));

  // Find the matching rule
  const { fixed, min, rate }: any = orderedRules.find(({ amount }, index) => {
    return amount > salary;
  });

  // Calculate tax for current category
  const tax = (fixed + (salary - min) * rate) / 12;

  return tax;
}